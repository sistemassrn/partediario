# Servicios web sanatoriales

Aquí concentraremos los servicios web intrasanatoriales desarrollados internamente.

Todo se monta en un servidor nginx, usando gunicorn como servidor de aplicaciones y flask como
framework de desarrollo.

Las direcciones servidas son:

- /parteInternados: Lista de pacientes internados en el momento para imprimir, y ordenado.
