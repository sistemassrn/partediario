import pyodbc

from jinja2 import Template
def partediariocompleto(): 
    """
    Función para devolver pacientes internados en formate parte diario
    """
    # Datos para abrir la conexion con el servidor MSSQL
    database = 'SRN'
    username = 'sa' 
    password = 'srn'
    server = 'tcp:192.168.5.130'

    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()

    # Obtengo los datos desde la BBDD
    query = "select C.HAB AS Sala,\
        C.SEC AS Sector,\
        C.CAM AS Cama,\
        I.ORI AS Origen,\
        I.PAC AS Paciente,\
        CAST(DATEDIFF(DAY,P.FNA,GETDATE())/365.25 AS INT) AS Edad,\
        P.NOM AS Nombre,\
        M.DES AS MotivoDeIngreso,\
        PR.NOM AS Profesional,\
        O.DES AS Institucion,\
        P.TIN AS TipInt,\
        convert(date,P.FIN) AS FechaIngreso,\
        DATEDIFF(DAY,P.FIN,GETDATE()) AS DiasEst from INTCAM C \
        LEFT JOIN INTEST E ON (C.HAB = E.HAB AND C.CAM = E.CAM AND E.FEC = convert(VARCHAR(10),GETDATE(),112)+' 23:59:59')\
        LEFT JOIN INTPAC I ON (E.ORI = I.ORI AND E.PAC = I.PAC and I.EGR = 'N')\
        LEFT JOIN CLIPAC P ON (I.ORI = P.ORI AND I.PAC = P.PAC)\
        LEFT JOIN INTPAT M ON (M.PAT = P.MOT)\
        LEFT JOIN CLINST O ON (O.INS = P.INS)\
        LEFT JOIN CLIPRF PR ON (PR.PRF = P.PRF)\
        WHERE C.OPE = 'S'" .format(campos='*')
            
    cursor.execute(query)
    parteDiario = cursor.fetchall()

    #Separacion de tablas para mostrar
    parteImprimir = [
        {"titulo": "Guardia - Urgencia", "contenido": [e for e in parteDiario if e[1] == "ST" ]},
        {"titulo": "Oncologia", "contenido": [e for e in parteDiario if e[1] == "ON" ]},
        {"titulo": "Hospital de día", "contenido": [e for e in parteDiario if e[1] == "on" ]},
        {"titulo": "Materno-Infantil Primer Piso 'A' ", "contenido": [e for e in parteDiario if e[1] == "PE" or e[1] == "P1" ]},
        {"titulo": "Terapia Neonatal", "contenido": [e for e in parteDiario if e[1] == "TN" ]},
        {"titulo": "Quirofano - Recuperacion", "contenido": [e for e in parteDiario if e[1] == "RE" ]},
        {"titulo": "Clinica Medica Primer Piso 'B' ", "contenido": [e for e in parteDiario if e[1] == "1B" ]},
        {"titulo": "Medicoquirurgico Segundo Piso 'A' ", "contenido": [e for e in parteDiario if e[1] == "2V" or e[1] == "P2" ]},
        {"titulo": "Terapia Intensiva Adultos ", "contenido": [e for e in parteDiario if e[1] == "TI" ]},
        {"titulo": "Terapia Intermedia Adultos ", "contenido": [e for e in parteDiario if e[1] == "TB" ]},
        ]


    with open("parteDiarioCompleto.tpl") as file_:
        tpl = Template(file_.read())
        
    return tpl.render(parteImprimir=parteImprimir)
