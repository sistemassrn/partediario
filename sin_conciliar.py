import pyodbc

from jinja2 import Template

def facturas_cobradas_sin_conciliar():
    # Datos para abrir la conexion con el servidor MSSQL
    database = 'SRN'
    username = 'sa' 
    password = 'srn'
    server = 'tcp:192.168.5.130'

    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()

    query = "SELECT distinct   P.INS, P.CPT, P.SUC, P.NFA FROM CAJMOV C RIGHT JOIN CLICCP P ON C.TIM=P.CPT AND P.SUC=C.SIM AND P.NFA=C.NIM WHERE P.FPA IS NULL AND P.FFA>'2018-01-01 00:00:00.000' AND C.CPT='PG'  AND C.DET LIKE'%HO%' AND C.REG>'2018-30-06 01:03:06.000' ORDER BY P.CPT, P.SUC DESC"

    cursor.execute(query)
    sin_conciliar = cursor.fetchall()

    return str(sin_conciliar)

