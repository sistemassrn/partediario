<HTML>
<HEAD>

<STYLE>
table.minimalistBlack {
  border: 1px solid #000000;
  width: 100%;
  text-align: left;
  border-collapse: collapse;
  font-family: monospace,monospace;
}
table.minimalistBlack td, table.minimalistBlack th {
  border: 1px solid #aaaaaa;
  padding: 2px 1px;
  text-align: center;
}
table.minimalistBlack tbody td {
  font-size: 8px;
}
table.minimalistBlack thead {
  background: #CFCFCF;
  background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
  background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
  background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
  border-bottom: 2px solid #888888;
}
table.minimalistBlack tbody td.titulo {
  background: #CFCFCF;
  background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
  background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
  background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
  border-bottom: 2px solid #888888;
  font-weight: bold;
  font-size: 12px;
}
table.minimalistBlack thead td {
  font-size: 10px;
}
table.minimalistBlack thead th {
  font-size: 8px;
  font-weight: bold;
  color: #000000;
  text-align: center;
}
table.minimalistBlack tfoot {
  font-size: 5px;
  font-weight: bold;
  color: #000000;
  border-top: 3px solid #000000;
}
table.minimalistBlack tfoot td {
  font-size: 5px;
}
</STYLE>

</HEAD>
<BODY>
    <table class="minimalistBlack">
    <thead>
        <tr>
            <td>Sala</td>
            <td>Sec</td>
            <td>Ca</td>
            <td>Ori</td>
            <td>Pac</td>
            <td>Ed.</td>
            <td>Nombre</td>
            <td>MotivoDeIngreso</td>
            <td>Profesional</td>
            <td>Institucion</td>
            <td>Tip</td>
            <td>FechaIngreso</td>
            <td>Est</td>
        </tr>
  </thead>
{% for sector in parteImprimir %}

  
  <tbody>
    <tr><td class="titulo" colspan="13">{{ sector.titulo }}<td></tr>
  {% for linea in sector.contenido %}
     <tr>
     {% for columna in linea %}
       <td>{{ columna }}</td>
     {% endfor %}
     
     </tr>
    {% endfor %}
  </tbody>

  
 {% endfor %}
  </table>
</body>
</html>