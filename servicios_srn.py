from flask import Flask
from pacientes_internados import pacientesinternados
from parte_diario_completo import partediariocompleto
from sin_conciliar import facturas_cobradas_sin_conciliar

app = Flask(__name__)


@app.route("/")
def main():
    return "<h1 style='color:blue'>Indice de servicios del SRN</h1>"


@app.route("/pacientesinternados")
def devolverPacientesInternados():
    return pacientesinternados()


@app.route("/partediario")
def devolverParteDiarioCompleto():
    return partediariocompleto()


@app.route("/facturasCobradasSinConciliar")
def facturasCobradasSinConciliar():
    return facturas_cobradas_sin_conciliar()


if __name__ == "__main__":
    app.run(host='0.0.0.0')
